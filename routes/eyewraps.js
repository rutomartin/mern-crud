const express = require('express');
const router = express.Router();
const RateLimit = require('express-rate-limit');
const mongoose = require('mongoose');
const stringCapitalizeName = require('string-capitalize-name');

const EyeWrap = require('../models/eyewrap');

const minutes = 5;
const postLimit = new RateLimit({
    windowMs: minutes * 60 * 1000,
    max: 100,
    delayMs: 0,
    handler: (req, res) => {
        res.status(429).json({ success: false, 
            msg: `Too many requests. please try again after ${minutes} minutes.`});
    }
});

router.get('/:id', (req, res) => {
    EyeWrap.findById(req.params.id)
    .then((result) => {
        res.json(result);
    })
    .catch((err) => {
        res.status(404).json({success: false, msg: `No such user.`});
    });
});

router.get('/', (req, res) => {
    EyeWrap.find({})
    .then((result) => {
        res.json(result);
    })
    .catch((err) => {
        res.status(500).json({success: false, msg: `Something went wrong. ${err}`});
    });
});

router.post('/', postLimit, (req, res) => {
    let eyeWrap = new EyeWrap({
        name: req.body.name,
        email: req.body.email,
        number: req.body.number,
        password: req.body.password,
        taxi: req.body.taxi
    });
    
    eyeWrap.save()
    .then(result => {
        res.json({
            success: true,
            msg: `Successfully added!`,
            result: {
                _id: result._id,
                name: result.name,
                email: result.email,
                number: result.number,
                password: result.password,
                taxi: result.taxi
            }
        });
    })
    .catch((err) => {
        res.status(500).send(err)
        if (err.errors) {
            res.status(500).json({success: false, msg: `Something went wrong ${err}`});
        }
    });
});

router.put('/:id', (req, res) => {
   let updateEyeWrap = {
    name: req.body.name,
    email: req.body.email,
    number: req.body.number,
    password: req.body.password,
    taxi: req.body.taxi
   };

   EyeWrap.findOneAndUpdate({_id: req.params.id}, updateEyeWrap, { runValidators: false,  context: 'query'})
   .then((oldResult) => {
       EyeWrap.findOne({_id: req.params.id})
       .then((newResult) => {
           res.json({
               success: true,
               msg: `Successfully updated`,
               result: {
                   _id: newResult._id,
                   name: newResult.name,
                   email: newResult.email,
                   number: newResult.number,
                   password: newResult.password,
                   taxi: newResult.taxi
               }
           });
       })
       .catch((err) => {
           res.status(500).json({success: false, msg: `Somethinf went wrong ${err}`});
           return;
       });
   })
   .catch((err) => {
        res.status(500).json({success: false, msg: `Somethinf went wrong ${err}`});
        return;
    });
});

router.post('/login', (req, res) => {
    console.log("log in ", req.body);

    EyeWrap.findOne({email: req.body.email})
    .then(result => {
        console.log("result ", result);
        console.log("password ", req.body.password);
        console.log("result.password ", result.password);
        if (req.body.password === result.password) {
            res.json({
                success: true,
               msg: `Successfully logged in`,
               result: {
                   _id: result._id,
                   name: result.name,
                   email: result.email,
                   number: result.number,
                   password: result.password,
                   taxi: result.taxi
               }
            });
        } else {
            res.status(401).json({success: false, msg: `Invalid credentials`});
        }
    })
    .catch((err) => {
        res.status(500).json({success: false, msg: `Something went wrong. ${err}`});
    });
});

router.delete('/:id', (req, res) => {
    EyeWrap.findByIdAndRemove(req.params.id)
    .then((result) => {
        res.json({
            success: true,
            msg: 'deleted',
            result: {
                _id: result._id,
                   name: result.name,
                   email: result.email,
                   number: result.number,
                   password: result.password,
                   taxi: result.taxi
            }
        });
    })
    .catch((err) => {
        result.status(404).json({ success: false, msg: `Nothing to delete`})
    });
});

module.exports = router;