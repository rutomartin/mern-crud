// Set the connection string based from the config vars of the production server
// To run locally use 'mongodb://localhost/mern-crud' instead of process.env.DB

module.exports = {
  db: 'mongodb://ruto:nitrammongo@kromsystemscluster-shard-00-00-b4dq3.mongodb.net:27017,kromsystemscluster-shard-00-01-b4dq3.mongodb.net:27017,kromsystemscluster-shard-00-02-b4dq3.mongodb.net:27017/test?ssl=true&replicaSet=KromSystemsCluster-shard-0&authSource=admin&retryWrites=true&w=majority'
};
