const mongoose = require('mongoose');
const unique = require('mongoose-unique-validator');
const validate = require('mongoose-validator');

const EyewrapSchema = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    number: {
        type: String
    },
    password: {
        type: String
    },
    taxi: {
        type: String
    }
});

const EyeWrap = module.exports = mongoose.model('EyeWrap', EyewrapSchema);